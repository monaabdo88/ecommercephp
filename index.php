<?php
	ob_start();
	session_start();
	$pageTitle = 'Homepage';
	include 'init.php';
	include $tpl."banner.php";
?>
<div class="latest-products">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="section-heading">
              <h2>Latest Products</h2>
              <a href="products.html">view all products <i class="fa fa-angle-right"></i></a>
            </div>
          </div>
		  <?php
			$allItems = getAllRecords('*', 'items', 'where Approve = 1', '', 'Item_ID');
			foreach ($allItems as $item) {
				echo '<div class="col-md-4">
						<div class="product-item">
							<a href="#"><img src="assets/images/product_01.jpg" alt=""></a>
							<div class="down-content">
								<a href="items.php?itemid='. $item['Item_ID'] .'">
									<h4>'.$item['Name'].'</h4>
								</a>
								<h6>'.$item['Price'].'</h6>
								<p>'.$item['Description'].'</p>
								<ul class="stars">
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
									<li><i class="fa fa-star"></i></li>
								</ul>
								<span>'.$item['Add_Date'].'</span>
						  </div>
							
						</div>
					</div>';
			}
		?>
        
         
        </div>
      </div>
    </div>
<?php
	include $tpl . 'footer.php'; 
	ob_end_flush();
?>