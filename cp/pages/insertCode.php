<?php
// Insert Page Page

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    echo "<h1 class='text-center'>Insert Page</h1>";
    echo "<div class='container'>";

    // Upload Variables

    $avatarName = $_FILES['avatar']['name'];
    $avatarSize = $_FILES['avatar']['size'];
    $avatarTmp	= $_FILES['avatar']['tmp_name'];
    $avatarType = $_FILES['avatar']['type'];

    // List Of Allowed File Typed To Upload

    $avatarAllowedExtension = array("jpeg", "jpg", "png", "gif");

    // Get Avatar Extension

    //$avatarExtension = strtolower(end(explode('.', $avatarName)));
    $tmp = explode('.',$avatarName);
    $avatarExtension = strtolower(end($tmp));
    // Get Variables From The Form

    $title 	= $_POST['title'];
    $content 	= $_POST['content'];
    
    // Validate The Form

    $formErrors = array();

    if (empty($title)) {
        $formErrors[] = 'Title Cant Be <strong>Empty</strong>';
    }

    if (empty($content)) {
        $formErrors[] = 'Content Cant Be <strong>Empty</strong>';
    }

    if (! empty($avatarName) && ! in_array($avatarExtension, $avatarAllowedExtension)) {
        $formErrors[] = 'This Extension Is Not <strong>Allowed</strong>';
    }

    if ($avatarSize > 4194304) {
        $formErrors[] = 'Avatar Cant Be Larger Than <strong>4MB</strong>';
    }

    // Loop Into Errors Array And Echo It

    foreach($formErrors as $error) {
        echo '<div class="alert alert-danger">' . $error . '</div>';
    }

    // Check If There's No Error Proceed The Update Operation

    if (empty($formErrors)) {

        if($_FILES['avatar']['size'] != 0){
            $avatar = rand(0, 10000000000) . '_' . $avatarName;
        }else{
            $avatar = '';
        }
        move_uploaded_file($avatarTmp, "uploads\pages\\" . $avatar);

        // Check If Page Exist in Database

        $check = checkItem("title", "pages", $title);

        if ($check == 1) {
            echo '
                                <script type="text/javascript">
                                    $(document).ready(function(){
                                        errorFn("Sorry This Page Is Exist","warning");

                                    });
                                    
                                </script>
                                ';
                                redirectPage('back');

            

        } else {

            // Insert Pageinfo In Database

            $stmt = $con->prepare("INSERT INTO 
                                        pages(title, content, main_image)
                                    VALUES(:ztitle, :zcontent, :zavatar) ");
            $stmt->execute(array(

                'ztitle' 	=> $title,
                'zcontent' 	=> $content,
                'zavatar'	=> $avatar

            ));

            // Echo Success Message

            echo '
                                <script type="text/javascript">
                                    $(document).ready(function(){
                                        successFn("' . $stmt->rowCount() . ' Record Inserted","success");

                                    });
                                    
                                </script>
                                ';
                                redirectPage('back');

        }

    }
}