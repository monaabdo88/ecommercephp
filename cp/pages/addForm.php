<h1 class="text-center">Add New Memeber</h1>
<form class="form-horizontal" action="?do=Insert" method="POST" enctype="multipart/form-data">
					<!-- Start title Field -->
					<div class="form-group form-group-lg">
						<label class="col-sm-2 control-label">Page title</label>
						<div class="col-sm-10 col-md-10">
							<input type="text" name="title" class="form-control" autocomplete="off" required="required" placeholder="Page title" />
						</div>
					</div>
					<!-- End title Field -->
					<!-- Start Content Field -->
					<div class="form-group form-group-lg">
						<label class="col-sm-2 control-label">Page Content</label>
						<div class="col-sm-10 col-md-10">
						    <textarea name="content"></textarea>
                        </div>
					</div>
					<!-- End Content Field -->
					
					<!-- Start Avatar Field -->
					<div class="form-group form-group-lg">
						<label class="col-sm-2 control-label">Main Image</label>
						<div class="col-sm-10 col-md-10">
							<input type="file" name="avatar" onchange="readURL(this);" class="form-control" />
						</div>
					</div>
                    <div class="col-md-6 col-md-offset-3">
                        <img id="preview" style="display:none" src="#" class="img-thumbnail img-responsive" />
                        <br/><br>
                    </div>
					<!-- End Avatar Field -->
					<!-- Start Submit Field -->
					<div class="form-group form-group-lg">
						<div class="col-sm-offset-2 col-sm-10">
							<input type="submit" value="Add Page" class="btn btn-primary btn-lg btn-block" />
						</div>
					</div>
					<!-- End Submit Field -->
				</form>
                <script src="https://cdn.ckeditor.com/4.16.1/standard/ckeditor.js"></script>
                <script>
                        CKEDITOR.replace( 'content' );
                </script>