<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $id = $_POST['id'];
    $stmt = $con->prepare("SELECT * FROM pages WHERE id = ? LIMIT 1");
    $stmt->execute(array($id));
    $row = $stmt->fetch();
    //start update data code
    $avatar = $row['main_image']; 
    // Get Variables From The Form

    $id 	= $_POST['id'];
    $title 	= $_POST['title'];
    $content 	= $_POST['content'];
   
    $formErrors = array();

    if (empty($title)) {
        $formErrors[] = 'Title Cant Be <strong>Empty</strong>';
    }

    if (empty($content)) {
        $formErrors[] = 'Content Cant Be <strong>Empty</strong>';
    }
    if($_FILES['main_image']['size'] != 0)
     {
        if($row['main_image'] != NULL)   
         unlink('uploads/pages/'.$row['main_image']);
         // Upload Variables

         $avatarName = $_FILES['main_image']['name'];
         $avatarSize = $_FILES['main_image']['size'];
         $avatarTmp	= $_FILES['main_image']['tmp_name'];
         $avatarType = $_FILES['main_image']['type'];

         // List Of Allowed File Typed To Upload

         $avatarAllowedExtension = array("jpeg", "jpg", "png", "gif");

         // Get Avatar Extension
         $tmp = explode('.',$avatarName);
         $avatarExtension = strtolower(end($tmp));
         if (empty($avatarName)) {
             $formErrors[] = 'Avatar Is <strong>Required</strong>';
         }
         if ($avatarSize > 4194304) {
             $formErrors[] = 'Avatar Cant Be Larger Than <strong>4MB</strong>';
         }
         $avatar = rand(0, 10000000000) . '_' . $avatarName;

         if(empty($formErrors))
         {
            if($_FILES['main_image']['size'] != 0){
                $avatar = rand(0, 10000000000) . '_' . $avatarName;
            }else{
                $avatar = null;
            }
             move_uploaded_file($avatarTmp, "uploads\pages\\" . $avatar);
         }
     }
    // Loop Into Errors Array And Echo It

    foreach($formErrors as $error) {
        echo '<div class="alert alert-danger">' . $error . '</div>';
    }

    // Check If There's No Error Proceed The Update Operation

    if (empty($formErrors)) {

        $stmt2 = $con->prepare("SELECT 
                                    *
                                FROM 
                                    pages
                                WHERE
                                    title = ?
                                AND 
                                    id != ?");

        $stmt2->execute(array($title, $id));

        $count = $stmt2->rowCount();

        if ($count == 1) {

            echo '
         <script type="text/javascript">
             $(document).ready(function(){
                 errorFn("Sorry This Page Is Exist","warning");

             });
             
         </script>
         ';
         redirectPage('back');

        } else { 

            // Update The Database With This Info

            $stmt = $con->prepare("UPDATE pages SET title = ?, content = ?, main_image = ?  WHERE id = ?");

            $stmt->execute(array($title, $content, $avatar, $id));

            // Echo Success Message

            echo '
            <script type="text/javascript">
                $(document).ready(function(){
                    successFn("' . $stmt2->rowCount() . ' Record Updated","success");
   
                });
                
            </script>
            ';
            redirectPage('back');

        }

    }

} else {
    echo '
         <script type="text/javascript">
             $(document).ready(function(){
                 errorFn("Sorry You Cant Browse This Page Directly","warning");

             });
             
         </script>
         ';
         redirectPage('back');

    

}