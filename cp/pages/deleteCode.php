<?php
$id = isset($_GET['id']) && is_numeric($_GET['id']) ? intval($_GET['id']) : 0;

$check = checkItem('id','pages',$id);
if($check > 0 )
{
    // To get user IMage link and delete from pages folder
    $imgStmt = $con->prepare("SELECT * FROM pages WHERE id = ?");
    $imgStmt->execute(array($id));
    $row = $imgStmt->fetch();
    //Delete Main Image
    if($row['main_image'] != NULL)
        unlink('uploads/pages/'.$row['main_image']);
    //start to delete from database
    $stmt = $con->prepare("DELETE FROM pages where id = :zuser");
    $stmt->bindParam(":zuser", $id);
    $stmt->execute();
    echo '
    <script type="text/javascript">
        $(document).ready(function(){
            successFn("Record Deleted","success");

        });
        
    </script>
    ';
    redirectPage('back');

}
else
{
    echo '
    <script type="text/javascript">
        $(document).ready(function(){
            errorFn("Sorry Error in deleting proccess","warning");

        });
        
    </script>
    ';
    redirectPage('back');
}