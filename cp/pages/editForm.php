<h1 class="text-center">Edit Page</h1>
			
					<form class="form-horizontal" action="?do=Update" method="POST" enctype="multipart/form-data">
						<input type="hidden" name="id" value="<?php echo $row['id'] ?>" />
						<!-- Start Title Field -->
						<div class="form-group form-group-lg">
							<label class="col-sm-2 control-label">Title</label>
							<div class="col-sm-10 col-md-10">
								<input type="text" name="title" class="form-control" value="<?php echo $row['title'] ?>" autocomplete="off" required="required" />
							</div>
						</div>
						<!-- End Title Field -->
						<!-- Start Content Field -->
						<div class="form-group form-group-lg">
							<label class="col-sm-2 control-label">Page Content</label>
							<div class="col-sm-10 col-md-10">
								<textarea name="content"><?=$row['content']?></textarea>
							</div>
						</div>
						<!-- End Content Field -->
                        <!----  image----->
                        <div class="form-group form-group-lg">
                            <label class="col-sm-2 control-label">Main Image</label>
                            <div class="col-sm-10 col-md-10">
                                <input type="file" name="main_image" onchange="readURL(this);" class="form-control" />
                            </div>
					    </div>
                        <div class="col-md-6 col-md-offset-3">
                            <img id="preview" src="<?= ($row['main_image'] != NULL) ? 'uploads/pages/'.$row['main_image'] : "#" ?>" class="img-thumbnail img-responsive" />
                            <br/><br>
                        </div>
						<!-- End Image Field -->
						<!-- Start Submit Field -->
						<div class="form-group form-group-lg">
							<div class="col-sm-offset-2 col-sm-10">
								<input type="submit" value="Save" class="btn btn-primary btn-lg btn-block" />
							</div>
						</div>
						<!-- End Submit Field -->
					</form>
					<script src="https://cdn.ckeditor.com/4.16.1/standard/ckeditor.js"></script>
                <script>
                        CKEDITOR.replace( 'content' );
                </script>			