<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8" />
		<title><?=getTitle();?></title>
		<link rel="stylesheet" href="<?php echo $css ?>bootstrap.min.css" />
		<link rel="stylesheet" href="<?php echo $css ?>font-awesome.min.css" />
		<link rel="stylesheet" href="<?php echo $css ?>jquery-ui.css" />
		<link rel="stylesheet" href="<?php echo $css ?>jquery.selectBoxIt.css" />
		<link rel="stylesheet" href="<?php echo $css ?>backend.css" />
		<script src="<?php echo $js ?>jquery-1.12.1.min.js"></script>
		<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
		<script type="text/javascript">
			//show Success Message
			function successFn(msg,status){
				swal("Good Job!", msg, status);
			}
			//show error message
			function errorFn(msg,status){
				swal("Error!", msg, status);
			}
</script>
	</head>
	<body>