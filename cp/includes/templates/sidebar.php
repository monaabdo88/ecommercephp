<div class="col-md-3 float-left">
    <ul class="sidebar-nav">
        <li><a href="../index.php">Home</a></li>
        <li class="<?=isActive('dashboard.php')?>"><a href="dashboard.php">Dashboard</a></li>
        <li class="<?=isActive('settings.php')?>"><a href="settings.php">Settings</a></li>
        <li class="<?=isActive('sliders.php')?>"><a href="sliders.php?do=Manage">Sliders</a></li>
        <li class="<?=isActive('members.php')?>"><a href="members.php?do=Manage">Memebers</a></li>
        <li class="<?=isActive('pages.php')?>"><a href="pages.php?do=Manage">Pages</a></li>
        <li class="<?=isActive('categories.php')?>"><a href="categories.php?do=Manage">Categories</a></li>
        <li class="<?=isActive('items.php')?>"><a href="items.php?do=Manage">Items</a></li>
        <li class="<?=isActive('comments.php')?>"><a href="comments.php?do=Manage">Comments</a></li>
        <li><a href="logout.php">Logout</a></li>
    </ul>
</div>