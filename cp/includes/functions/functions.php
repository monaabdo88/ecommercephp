<?php
//Function to Get All Records From database
function getAllRecords($field,$tbl,$where= NULL,$and = NULL,$orderField,$ordering='DESC')
{
    global $con;
    $getAll = $con->prepare("SELECT $field FROM $tbl $where $and ORDER BY $orderField $ordering");
    $getAll->execute();
    $all = $getAll->fetchAll();
    return $all;
}
//Function to print The page Title
function getTitle()
{
    global $pageTitle;
    if(isset($pageTitle)){
        echo $pageTitle;
    }else{
        echo 'Default';
    }
}
//Redirect Function
function redirectPage($url= NULL, $seconds = 5){
    if($url === NULL)
    {
        $url = "index.php";
        $link = "HomePage";
    }
    else{
        if (isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER'] !== '') {
            $url = $_SERVER['HTTP_REFERER'];
            $link = "Previous Page";
        }else{
            $url = "index.php";
            $link = "HomePage";
        }
    }
    header("refresh:$seconds,url=$url");
    exit();
}
//Function To check if item exist or not
function checkItem($select,$tbl,$value){
    global $con;
    $stmt = $con->prepare("SELECT $select FROM $tbl WHERE $select = ?");
    $stmt->execute(array($value));
    $count = $stmt->rowCount();
    return $count;
}
//Function to get count of rows
function countItems($item,$tbl){
    global $con;
    $stmt = $con->prepare("SELECT COUNT($item) FROM $tbl");
    $stmt->execute();
    return $stmt->fetchColumn();
}
//function to get latest items from database
function getLatest($field,$tbl,$order,$limit = 10){
    global $con;
    $stmt = $con->prepare("SELECT $field FROM $tbl ORDER BY $order DESC LIMIT $limit");
    $stmt->execute();
    $rows = $stmt->fetchAll();
    return $rows;

}
//Active Link in sidebar dashboard
function isActive($page){
    $uri_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
    $uri_segments = explode('/', $uri_path);
    if($uri_segments[2] == $page)
    {
         return 'is-active';
    }
    else
    {
         return '';
    }

}