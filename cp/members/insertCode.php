<?php
// Insert Member Page

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    echo "<h1 class='text-center'>Insert Member</h1>";
    echo "<div class='container'>";

    // Upload Variables

    $avatarName = $_FILES['avatar']['name'];
    $avatarSize = $_FILES['avatar']['size'];
    $avatarTmp	= $_FILES['avatar']['tmp_name'];
    $avatarType = $_FILES['avatar']['type'];

    // List Of Allowed File Typed To Upload

    $avatarAllowedExtension = array("jpeg", "jpg", "png", "gif");

    // Get Avatar Extension

    //$avatarExtension = strtolower(end(explode('.', $avatarName)));
    $tmp = explode('.',$avatarName);
    $avatarExtension = strtolower(end($tmp));
    // Get Variables From The Form

    $user 	= $_POST['username'];
    $pass 	= $_POST['password'];
    $email 	= $_POST['email'];
    $name 	= $_POST['full'];

    $hashPass = sha1($_POST['password']);

    // Validate The Form

    $formErrors = array();

    if (strlen($user) < 4) {
        $formErrors[] = 'Username Cant Be Less Than <strong>4 Characters</strong>';
    }

    if (strlen($user) > 20) {
        $formErrors[] = 'Username Cant Be More Than <strong>20 Characters</strong>';
    }

    if (empty($user)) {
        $formErrors[] = 'Username Cant Be <strong>Empty</strong>';
    }

    if (empty($pass)) {
        $formErrors[] = 'Password Cant Be <strong>Empty</strong>';
    }

    if (empty($name)) {
        $formErrors[] = 'Full Name Cant Be <strong>Empty</strong>';
    }

    if (empty($email)) {
        $formErrors[] = 'Email Cant Be <strong>Empty</strong>';
    }

    if (! empty($avatarName) && ! in_array($avatarExtension, $avatarAllowedExtension)) {
        $formErrors[] = 'This Extension Is Not <strong>Allowed</strong>';
    }

    if (empty($avatarName)) {
        $formErrors[] = 'Avatar Is <strong>Required</strong>';
    }

    if ($avatarSize > 4194304) {
        $formErrors[] = 'Avatar Cant Be Larger Than <strong>4MB</strong>';
    }

    // Loop Into Errors Array And Echo It

    foreach($formErrors as $error) {
        echo '<div class="alert alert-danger">' . $error . '</div>';
    }

    // Check If There's No Error Proceed The Update Operation

    if (empty($formErrors)) {

        $avatar = rand(0, 10000000000) . '_' . $avatarName;

        move_uploaded_file($avatarTmp, "uploads\avatars\\" . $avatar);

        // Check If User Exist in Database

        $check = checkItem("Username", "users", $user);

        if ($check == 1) {
            echo '
                                <script type="text/javascript">
                                    $(document).ready(function(){
                                        errorFn("Sorry This User Is Exist","warning");

                                    });
                                    
                                </script>
                                ';
                                redirectPage('back');

            

        } else {

            // Insert Userinfo In Database

            $stmt = $con->prepare("INSERT INTO 
                                        users(Username, Password, Email, FullName, RegStatus, Date, avatar)
                                    VALUES(:zuser, :zpass, :zmail, :zname, 1, now(), :zavatar) ");
            $stmt->execute(array(

                'zuser' 	=> $user,
                'zpass' 	=> $hashPass,
                'zmail' 	=> $email,
                'zname' 	=> $name,
                'zavatar'	=> $avatar

            ));

            // Echo Success Message

            echo '
                                <script type="text/javascript">
                                    $(document).ready(function(){
                                        successFn("' . $stmt->rowCount() . ' Record Inserted","success");

                                    });
                                    
                                </script>
                                ';
                                redirectPage('back');

        }

    }
}