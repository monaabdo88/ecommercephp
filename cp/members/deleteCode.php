<?php
$userID = isset($_GET['id']) && is_numeric($_GET['id']) ? intval($_GET['id']) : 0;

$check = checkItem('UserID','users',$userID);
if($check > 0 )
{
    // To get user IMage link and delete from users folder
    $imgStmt = $con->prepare("SELECT * FROM users WHERE UserID = ?");
    $imgStmt->execute(array($userID));
    $row = $imgStmt->fetch();
    if($row['avatar'] != NULL)
        unlink('uploads/avatars/'.$row['avatar']);
    //start to delete from database
    $stmt = $con->prepare("DELETE FROM users where UserID = :zuser");
    $stmt->bindParam(":zuser", $userID);
    $stmt->execute();
    echo '
    <script type="text/javascript">
        $(document).ready(function(){
            successFn("Record Deleted","success");

        });
        
    </script>
    ';
    redirectPage('back');

}
else
{
    echo '
    <script type="text/javascript">
        $(document).ready(function(){
            errorFn("Sorry Error in deleting proccess","warning");

        });
        
    </script>
    ';
    redirectPage('back');
}