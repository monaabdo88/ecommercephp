<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $userID = $_POST['userid'];
    $stmt = $con->prepare("SELECT * FROM users WHERE UserID = ? LIMIT 1");
    $stmt->execute(array($userID));
    $row = $stmt->fetch();
    //start update data code
    $avatar = $row['avatar']; 
    // Get Variables From The Form

    $id 	= $_POST['userid'];
    $user 	= $_POST['username'];
    $email 	= $_POST['email'];
    $name 	= $_POST['full'];

    // Password Trick

    $pass = empty($_POST['newpassword']) ? $_POST['oldpassword'] : sha1($_POST['newpassword']);

    // Validate The Form

    $formErrors = array();

    if (strlen($user) < 4) {
        $formErrors[] = 'Username Cant Be Less Than <strong>4 Characters</strong>';
    }

    if (strlen($user) > 20) {
        $formErrors[] = 'Username Cant Be More Than <strong>20 Characters</strong>';
    }

    if (empty($user)) {
        $formErrors[] = 'Username Cant Be <strong>Empty</strong>';
    }

    if (empty($name)) {
        $formErrors[] = 'Full Name Cant Be <strong>Empty</strong>';
    }

    if (empty($email)) {
        $formErrors[] = 'Email Cant Be <strong>Empty</strong>';
    }
    if($_FILES['avatar']['size'] != 0)
     {
         unlink('uploads/avatars/'.$row['avatar']);
         // Upload Variables

         $avatarName = $_FILES['avatar']['name'];
         $avatarSize = $_FILES['avatar']['size'];
         $avatarTmp	= $_FILES['avatar']['tmp_name'];
         $avatarType = $_FILES['avatar']['type'];

         // List Of Allowed File Typed To Upload

         $avatarAllowedExtension = array("jpeg", "jpg", "png", "gif");

         // Get Avatar Extension
         $tmp = explode('.',$avatarName);
         $avatarExtension = strtolower(end($tmp));
         if (empty($avatarName)) {
             $formErrors[] = 'Avatar Is <strong>Required</strong>';
         }
         if ($avatarSize > 4194304) {
             $formErrors[] = 'Avatar Cant Be Larger Than <strong>4MB</strong>';
         }
         $avatar = rand(0, 10000000000) . '_' . $avatarName;

         if(empty($formErrors))
         {
             move_uploaded_file($avatarTmp, "uploads\avatars\\" . $avatar);
         }
     }
    // Loop Into Errors Array And Echo It

    foreach($formErrors as $error) {
        echo '<div class="alert alert-danger">' . $error . '</div>';
    }

    // Check If There's No Error Proceed The Update Operation

    if (empty($formErrors)) {

        $stmt2 = $con->prepare("SELECT 
                                    *
                                FROM 
                                    users
                                WHERE
                                    Username = ?
                                AND 
                                    UserID != ?");

        $stmt2->execute(array($user, $id));

        $count = $stmt2->rowCount();

        if ($count == 1) {

            echo '
         <script type="text/javascript">
             $(document).ready(function(){
                 errorFn("Sorry This User Is Exist","warning");

             });
             
         </script>
         ';
         redirectPage('back');

        } else { 

            // Update The Database With This Info

            $stmt = $con->prepare("UPDATE users SET Username = ?, Email = ?, FullName = ?,avatar = ? ,Password = ? WHERE UserID = ?");

            $stmt->execute(array($user, $email, $name,$avatar, $pass, $id));

            // Echo Success Message

            echo '
            <script type="text/javascript">
                $(document).ready(function(){
                    successFn("' . $stmt2->rowCount() . ' Record Updated","success");
   
                });
                
            </script>
            ';
            redirectPage('back');

        }

    }

} else {
    echo '
         <script type="text/javascript">
             $(document).ready(function(){
                 errorFn("Sorry You Cant Browse This Page Directly","warning");

             });
             
         </script>
         ';
         redirectPage('back');

    

}