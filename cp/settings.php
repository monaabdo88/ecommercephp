<?php
	ob_start(); // Output Buffering Start

	session_start();

	$pageTitle = 'Settings';

	if (isset($_SESSION['Username'])) {

		include 'init.php';

		$do = isset($_GET['do']) ? $_GET['do'] : 'Manage';

		// Start Manage Page

		
			$stmt = $con->prepare("SELECT * FROM settings WHERE id = 1 ");

			// Execute The Statement

			$stmt->execute();

			// Assign To Variable 
			$row = $stmt->fetch();
			?>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                    <?php include "includes/templates/sidebar.php" ?>
                    <div class="col-md-8 float-right">
                    
                        <h1 class="text-center">Edit Settings</h1>        
                        <?php 
                        //Edit Form
                        include ("settings/editForm.php");
                        //update settings
                        if($_SERVER['REQUEST_METHOD'] == 'POST'){
                            include("settings/updateCode.php");
                        }
                        
                        ?>
                        </div>
                    
                        
                    </div><!------- end col-md-12 ---------------->
                    
                    </div><!----- end row ---->
            </div><!----end container ----->
             
            
	<?php

		include $tpl . 'footer.php';
    ?>
    <!---- preview image before upload code ----->
    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                $('#preview').attr('src', e.target.result);
            }

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
    <?php
	} else {

		header('Location: index.php');

		exit();
	}

	ob_end_flush(); // Release The Output

?>