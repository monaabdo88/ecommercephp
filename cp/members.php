<?php
ob_start();
session_start();
$pageTitle="Members";
if(isset($_SESSION['Username']))
{
    include "init.php";
    $do = isset($_GET['do']) ? $_GET['do'] :'Manage';
    //Start Manage Pag
    
    ?>
    <div class="container">
                <div class="row">
                    <div class="col-md-12">
                    <?php include "includes/templates/sidebar.php" ?>
                    <div class="col-md-8 float-right">
    <?php
    if($do == 'Manage')
    {
        $query = '';
        if(isset($_GET['page']) && $_GET['page'] == 'pending')
        {
            $query = 'AND RegStatus = 0';
        }
        //Get All Users from database except admin
        $stmt = $con->prepare("SELECT * FROM users WHERE GroupID != 1 $query ORDER BY UserID DESC");
        $stmt->execute();
        $rows = $stmt->fetchAll();
        include("members/manage.php");

    }
    elseif($do == 'Add')
    {
        //Add  New User Form
        include("members/addForm.php");
    }
    elseif($do == 'Insert')
    {
        //Add New User Code
        include("members/insertCode.php");
    }
    elseif($do =='Edit')
    {
        $userId = isset($_GET['id']) && is_numeric($_GET['id']) ? intval($_GET['id']) : 0;
        $stmt = $con->prepare("SELECT * FROM users WHERE UserID = ? LIMIT 1");
        $stmt->execute(array($userId));
        $row = $stmt->fetch();
        $count = $stmt->rowCount();
        if($count > 0 )
            {
                //Edit Slider Form
                include ("members/editForm.php");
            }
            else {
                $theMsg = '<div class="alert alert-danger"><p class="text-center">Theres No Such ID</p></div>';
                redirectPage($theMsg);
            }
    }
    elseif($do == 'Update')
    {
        include ("members/updateCode.php");  
    }
    elseif($do == 'Delete')
    {
        include("members/deleteCode.php");
    }
    ?>
                        </div><!--- end col-md-8 ------>
                
                </div><!------- end col-md-12 ---------------->
            
            </div><!----- end row ---->
    </div><!----end container ----->
    
<?php
include $tpl . 'footer.php';
?>
<script type="text/javascript">
    //Preview Image Before Upload
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $("#preview").show();
                $('#preview').attr('src', e.target.result);

            }

                reader.readAsDataURL(input.files[0]);
            }
        }
        </script>
<?php
	} else {

		header('Location: index.php');

		exit();
	}

	ob_end_flush(); // Release The Output

?>