<?php
//Connect Database Files
require "connect.php";
//Functions File
$func = "includes/functions/";
//Templates Path
$tpl = "includes/templates/";
//Css Path
$css = "layout/css/";
//Js Path
$js = "layout/js/";
//Lanf Path
$lang = "includes/langs/";
// Include The Important Files
include $func . 'functions.php';
include $lang . 'english.php';
include $tpl . 'header.php';