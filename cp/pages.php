<?php
ob_start();
session_start();
$pageTitle="Pages";
if(isset($_SESSION['Username']))
{
    include "init.php";
    $do = isset($_GET['do']) ? $_GET['do'] :'Manage';
    //Start Manage Pag
    
    ?>
    <div class="container">
                <div class="row">
                    <div class="col-md-12">
                    <?php include "includes/templates/sidebar.php" ?>
                    <div class="col-md-8 float-right">
    <?php
    if($do == 'Manage')
    {
        $query = '';
        
        //Get All pages from database except admin
        $stmt = $con->prepare("SELECT * FROM pages ORDER BY id DESC");
        $stmt->execute();
        $rows = $stmt->fetchAll();
        include("pages/manage.php");

    }
    elseif($do == 'Add')
    {
        //Add  New User Form
        include("pages/addForm.php");
    }
    elseif($do == 'Insert')
    {
        //Add New User Code
        include("pages/insertCode.php");
    }
    elseif($do =='Edit')
    {
        $id = isset($_GET['id']) && is_numeric($_GET['id']) ? intval($_GET['id']) : 0;
        $stmt = $con->prepare("SELECT * FROM pages WHERE id = ? LIMIT 1");
        $stmt->execute(array($id));
        $row = $stmt->fetch();
        $count = $stmt->rowCount();
        if($count > 0 )
            {
                //Edit Slider Form
                include ("pages/editForm.php");
            }
            else {
                $theMsg = '<div class="alert alert-danger"><p class="text-center">Theres No Such ID</p></div>';
                redirectPage($theMsg);
            }
    }
    elseif($do == 'Update')
    {
        include ("pages/updateCode.php");  
    }
    elseif($do == 'Delete')
    {
        include("pages/deleteCode.php");
    }
    ?>
                        </div><!--- end col-md-8 ------>
                
                </div><!------- end col-md-12 ---------------->
            
            </div><!----- end row ---->
    </div><!----end container ----->
    
<?php
include $tpl . 'footer.php';
?>
<script type="text/javascript">
    //Preview Image Before Upload
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $("#preview").show();
                $('#preview').attr('src', e.target.result);

            }

                reader.readAsDataURL(input.files[0]);
            }
        }
        </script>
<?php
	} else {

		header('Location: index.php');

		exit();
	}

	ob_end_flush(); // Release The Output

?>