<?php
	ob_start(); // Output Buffering Start
	session_start();
	$pageTitle = 'Sliders';
	if (isset($_SESSION['Username'])) {
		include 'init.php';
		$do = isset($_GET['do']) ? $_GET['do'] : 'Manage';
            ?>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                    <?php include "includes/templates/sidebar.php" ?>
                    <div class="col-md-8 float-right">
                    <?php 
                    // Start Manage Page
                if($do == 'Manage'){
                        $stmt = $con->prepare("SELECT * FROM sliders ORDER BY id DESC ");

                        // Execute The Statement

                        $stmt->execute();

                        // Assign To Variable 
                        $rows = $stmt->fetchAll();
                    if(!empty($rows))
                    {
                        include ("sliders/manage.php");
                    }
                    else
                    {
                        echo '<div class="nice-message">There\'s No Sliders To Show</div>';
                        echo '<a href="sliders.php?do=Add" class="btn btn-primary">
                                <i class="fa fa-plus"></i> New Slider
                            </a>';
                    }
                }//End Manage Section
                elseif($do == 'Add')
                {
                    //Add New Slider Form
                    include ("sliders/addForm.php");
                }
                //Insert New Slider Section
                elseif($do == 'Insert')
                {
                    //Insret New Slider Code
                    include ("sliders/insert.php");   
                }
                //Edit Slider Section
                elseif($do == 'Edit')
                {
                    $sliderId = isset($_GET['id']) && is_numeric($_GET['id']) ? intval($_GET['id']) : 0;
                    $stmt = $con->prepare("SELECT * FROM sliders WHERE id = ? LIMIT 1");
                    $stmt->execute(array($sliderId));
                    $row = $stmt->fetch();
                    $count = $stmt->rowCount();
                    if($count > 0 )
                    {
                        //Edit Slider Form
                        include ("sliders/editForm.php");
                    }
                    else {
                        $theMsg = '<div class="alert alert-danger"><p class="text-center">Theres No Such ID</p></div>';
                        redirectPage($theMsg);

                    }
                    
                }//Update Code Section
                elseif($do == 'Update')
                {
                    include ("sliders/update.php");             

                }
                //Delete Code Section
                elseif($do == 'Delete')
                {
                    include("sliders/deleteCode.php");
                }
			?>
                        </div><!--- end col-md-8 ------>
                
                        </div><!------- end col-md-12 ---------------->
                    
                    </div><!----- end row ---->
            </div><!----end container ----->
            
	<?php
		include $tpl . 'footer.php';
    ?>
    <!---- preview image before upload code ----->
    <script type="text/javascript">
    //Preview Image Before Upload
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $("#preview").show();
                $('#preview').attr('src', e.target.result);

            }

                reader.readAsDataURL(input.files[0]);
            }
        }
    //Confirm Before Delete Slider
    $(document).ready(function(){
        $(document).on('click', '.confirm-del', function(e){
            var productId = $(this).data('id');
            SwalDelete(productId);
            e.preventDefault();
        });
    
    });
    
    
    </script>
    <?php
	} else {

		header('Location: index.php');

		exit();
	}

	ob_end_flush(); // Release The Output

?>