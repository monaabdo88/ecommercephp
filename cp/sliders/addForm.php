<h1 class="text-center">Add New Slider</h1>
            <form class="form-horizontal" action="?do=Insert" method="POST" enctype="multipart/form-data">
					<!-- Start Title Field -->
					<div class="form-group form-group-lg">
						<label class="col-sm-2 control-label">Title</label>
						<div class="col-sm-10 col-md-8">
							<input type="text" name="title" class="form-control" autocomplete="off" required="required" placeholder="Slider Title" />
						</div>
					</div>
					<!-- End Username Field -->
                    <!-- Start slug Field -->
                    <div class="form-group form-group-lg">
						<label class="col-sm-2 control-label">Slug</label>
						<div class="col-sm-10 col-md-8">
							<input type="text" name="slug" class="form-control" autocomplete="off" required="required" placeholder="Slider Slug" />
						</div>
					</div>
					<!-- End slug Field -->
					<!-- Start Avatar Field -->
					<div class="form-group form-group-lg">
						<label class="col-sm-2 control-label">Slider Image</label>
						<div class="col-sm-10 col-md-8">
							<input type="file" name="avatar" onchange="readURL(this);" class="form-control" required="required" />
						</div>
					</div>
                    <div class="col-md-6 col-md-offset-3">
                        <img id="preview" style="display:none" src="<?=$row['site_logo']?>" class="img-thumbnail img-responsive" />
                        <br/><br>
                    </div>
					<!-- End Avatar Field -->
					<!-- Start Submit Field -->
					<div class="form-group form-group-lg">
						<div class="col-md-6 col-md-offset-3">
							<input type="submit" value="Add Slider" class="btn btn-primary btn-block btn-lg" />
						</div>
					</div>
					<!-- End Submit Field -->
				</form>     