<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST') 
                {
                    // Upload Variables

                    $avatarName = $_FILES['avatar']['name'];
                    $avatarSize = $_FILES['avatar']['size'];
                    $avatarTmp	= $_FILES['avatar']['tmp_name'];
                    $avatarType = $_FILES['avatar']['type'];

                    // List Of Allowed File Typed To Upload

                    $avatarAllowedExtension = array("jpeg", "jpg", "png", "gif");

                    // Get Avatar Extension
                    $tmp = explode('.',$avatarName);
                    $avatarExtension = strtolower(end($tmp));
                    $title = $_POST['title'];
                    $slug = $_POST['slug'];
                    $formErrors = array();
                    if(empty($title)){
                        $formErrors[] ='Title is Required';
                    }
                    if(empty($slug)){
                        $formErrors[] ='Slug is Required';
                    }
                    if (empty($avatarName)) {
                        $formErrors[] = 'Avatar Is <strong>Required</strong>';
                    }
    
                    if ($avatarSize > 4194304) {
                        $formErrors[] = 'Avatar Cant Be Larger Than <strong>4MB</strong>';
                    }


                    if (empty($formErrors)) {

                        $avatar = rand(0, 10000000000) . '_' . $avatarName;
    
                        move_uploaded_file($avatarTmp, "uploads\sliders\\" . $avatar);
                    
                            // Insert Slider info In Database
    
                            $stmt = $con->prepare("INSERT INTO 
                                                        sliders(title,slug, banner)
                                                    VALUES(:title, :slug, :banner) ");
                            $stmt->execute(array(
    
                                'title' 	=> $title,
                                'slug' 	    => $slug,
                                'banner' 	=> $avatar
    
                            ));
    
                            // Echo Success Message
                            echo '
                                <script type="text/javascript">
                                    $(document).ready(function(){
                                        successFn("' . $stmt->rowCount() . ' Record Inserted","success");

                                    });
                                    
                                </script>
                                ';
                                redirectPage('back');
                    }
                    else
                    {
                        // Loop Into Errors Array And Echo It
                            
                        foreach($formErrors as $error) {
                            echo '<div class="alert alert-danger"><p class="text-center">' . $error . '</p></div>';
                        }
                    }
                }    
?>