<h1 class="text-center">Manage Sliders</h1>
                        <div class="table-responsive">
                        <a href="sliders.php?do=Add" class="btn btn-primary">
                            <i class="fa fa-plus"></i> New Slider
                        </a><br><br>
                            <table class="main-table manage-members text-center table table-bordered">
                                <tr>
                                    <td>#ID</td>
                                    <td>Avatar</td>
                                    <td>Title</td>
                                    <td>Control</td>
                                </tr>
                                <?php
                                    $x = 1;
                                    foreach($rows as $row) {
                                        echo "<tr>";
                                            echo "<td>" . $x. "</td>";
                                            echo "<td><img src='uploads/sliders/".$row['banner']."' class='img-responsive img-thumbnail' /></td>";

                                            echo "<td>" . $row['title'] . "</td>";
                                            echo "<td>
                                                <a href='sliders.php?do=Edit&id=" . $row['id'] . "' class='btn btn-success'><i class='fa fa-edit'></i> Edit</a>
                                                <a href='sliders.php?do=Delete&id=" . $row['id'] . "' data-id='".$row['id']."' class='btn btn-danger confirm-del'><i class='fa fa-close'></i> Delete </a>";
                                                
                                            echo "</td>";
                                        echo "</tr>";
                                        $x++;
                                    }
                                ?>
                                <tr>
                            </table>
                        </div>
<script>
$(document).ready(function(){
    $('.confirm-del').on('click', function(e){
        e.preventDefault(); //cancel default action

        //Recuperate href value
        var href = $(this).attr('href');
        var message = $(this).data('confirm');

        //pop up
        swal({
            title: "Are you sure you want to delete This Record?",
            text: message, 
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            /*swal("Poof! Your imaginary file has been deleted!", {
              icon: "success",
            });*/
            window.location.href = href;
          } else {
            swal("Your imaginary file is safe!");
          }
        });
    });
});
</script>