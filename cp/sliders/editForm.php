<h1 class="text-center">Edit Slider <br><?=$row['title']?></h1>
            <form class="form-horizontal" action="?do=Update" method="POST" enctype="multipart/form-data">
					<!-- Start Title Field -->
					<input type="hidden" name="id" value="<?=$row['id']?>"/>
					<div class="form-group form-group-lg">
						<label class="col-sm-2 control-label">Title</label>
						<div class="col-sm-10 col-md-8">
							<input type="text" name="title" class="form-control" value="<?=$row['title']?>" />
						</div>
					</div>
					<!-- End Username Field -->
                    <!-- Start slug Field -->
                    <div class="form-group form-group-lg">
						<label class="col-sm-2 control-label">Slug</label>
						<div class="col-sm-10 col-md-8">
							<input type="text" name="slug" class="form-control"  value="<?=$row['slug']?>" />
						</div>
					</div>
					<!-- End slug Field -->
					<!-- Start Avatar Field -->
					<div class="form-group form-group-lg">
						<label class="col-sm-2 control-label">Slider Image</label>
						<div class="col-sm-10 col-md-8">
							<input type="file" name="avatar" onchange="readURL(this);" class="form-control" />
						</div>
					</div>
                    <div class="col-md-6 col-md-offset-3">
                        <img id="preview" src="uploads/sliders/<?=$row['banner']?>" class="img-thumbnail img-responsive" />
                        <br/><br>
                    </div>
					<!-- End Avatar Field -->
					<!-- Start Submit Field -->
					<div class="form-group form-group-lg">
						<div class="col-md-6 col-md-offset-3">
							<input type="submit" value="Edit Slider" class="btn btn-primary btn-block btn-lg" />
						</div>
					</div>
					<!-- End Submit Field -->
				</form>     