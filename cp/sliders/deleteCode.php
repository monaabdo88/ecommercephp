<?php
$sliderID = isset($_GET['id']) && is_numeric($_GET['id']) ? intval($_GET['id']) : 0;

$check = checkItem('id','sliders',$sliderID);
if($check > 0 )
{
    // To get Slider IMage link and delete from sliders folder
    $imgStmt = $con->prepare("SELECT * FROM sliders WHERE id = ?");
    $imgStmt->execute(array($sliderID));
    $row = $imgStmt->fetch();
    unlink('uploads/sliders/'.$row['banner']);
    //start to delete from database
    $stmt = $con->prepare("DELETE FROM sliders where id = :zslider");
    $stmt->bindParam(":zslider", $sliderID);
    $stmt->execute();
    echo '
    <script type="text/javascript">
        $(document).ready(function(){
            successFn("Record Deleted","success");

        });
        
    </script>
    ';
    redirectPage('back');

}
else
{
    echo '
    <script type="text/javascript">
        $(document).ready(function(){
            errorFn("Sorry Error in deleting proccess","warning");

        });
        
    </script>
    ';
    redirectPage('back');
}