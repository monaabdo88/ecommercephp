<?php

$sliderId = $_POST['id'];
$stmt = $con->prepare("SELECT * FROM sliders WHERE id = ? LIMIT 1");
$stmt->execute(array($sliderId));
$row = $stmt->fetch();
//start update data code
$avatar = $row['banner'];                
$title = $_POST['title'];
$slug = $_POST['slug'];
 //errors array
 $formErrors = array();

     if($_FILES['avatar']['size'] != 0)
     {
         unlink('uploads/sliders/'.$row['banner']);
         // Upload Variables

         $avatarName = $_FILES['avatar']['name'];
         $avatarSize = $_FILES['avatar']['size'];
         $avatarTmp	= $_FILES['avatar']['tmp_name'];
         $avatarType = $_FILES['avatar']['type'];

         // List Of Allowed File Typed To Upload

         $avatarAllowedExtension = array("jpeg", "jpg", "png", "gif");

         // Get Avatar Extension
         $tmp = explode('.',$avatarName);
         $avatarExtension = strtolower(end($tmp));
         if (empty($avatarName)) {
             $formErrors[] = 'Avatar Is <strong>Required</strong>';
         }
         if ($avatarSize > 4194304) {
             $formErrors[] = 'Avatar Cant Be Larger Than <strong>4MB</strong>';
         }
         $avatar = rand(0, 10000000000) . '_' . $avatarName;
        if(empty($formErrors))
        {
            move_uploaded_file($avatarTmp, "uploads\sliders\\" . $avatar);
        }  
     }
     
     //show errors
     foreach($formErrors as $error) {
         echo '<div class="alert alert-danger"><p class="text-center">' . $error . '</p></div>';
     }
     //start update code
     if(empty($formErrors))
     {
         $stmt2 = $con->prepare("UPDATE sliders SET title = ? , banner = ? ,slug = ?  WHERE id = ?");
         $stmt2->execute(array($title,$avatar,$slug,$sliderId));
         echo '
         <script type="text/javascript">
             $(document).ready(function(){
                 successFn("' . $stmt2->rowCount() . ' Record Updated","success");

             });
             
         </script>
         ';
         redirectPage('back');
     }