<?php

	function lang($phrase) {

		static $lang = array(

			// Navbar Links

			'HOME_ADMIN' 	=> 'الرئيسية',
			'CATEGORIES' 	=> 'الأقسام',
			'ITEMS' 		=> 'Items',
			'MEMBERS' 		=> 'الأعضاء',
			'COMMENTS'		=> 'التعليقات',
			'STATISTICS' 	=> 'Statistics',
			'LOGS' 			=> 'Logs',
			'' => '',
			'' => '',
			'' => '',
			'' => '',
			'' => ''
		);

		return $lang[$phrase];

	}
