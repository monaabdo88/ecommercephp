<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="<?=getSettings('site_desc')?>">
    <meta name="tags" content="<?=getSettings('site_tags')?>" />
    <meta name="author" content="">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900&display=swap" rel="stylesheet">

    <title><?= getSettings('site_name'); ?></title>

    <!-- Bootstrap core CSS -->
    <link href="<?=$vendor?>bootstrap/css/bootstrap.min.css" rel="stylesheet">
<!--

TemplateMo 546 Sixteen Clothing

https://templatemo.com/tm-546-sixteen-clothing

-->

    <!-- Additional CSS Files -->
    <link rel="stylesheet" href="<?=$assets?>css/fontawesome.css">
    <link rel="stylesheet" href="<?=$assets?>css/templatemo-sixteen.css">
    <link rel="stylesheet" href="<?=$assets?>css/owl.css">
	<link rel="stylesheet" href="<?=$css?>front.css">
  </head>

  <body>
  <?php 
  if(getSettings('site_status') == 0){
    echo '<div class="container">
              <div class="row">
                <div class="col-md-8 col-md-offset-2 alert alert-danger" style="margin:150px auto;">
                  <p class="text-center">'.getSettings('site_text_close').'</p>
                </div>
              </div>
            </div>';
    die();
  }
  ?>
    <!-- ***** Preloader Start ***** -->
    <div id="preloader">
        <div class="jumper">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>  
    <!-- ***** Preloader End ***** -->

    <!-- Header -->
    <header class="">
      <nav class="navbar navbar-expand-lg">
        <div class="container">
          <a class="navbar-brand" href="index.php"><h2><?=getSettings('site_name')?></h2></a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
              <li class="nav-item active">
                <a class="nav-link" href="index.php">Home
                  <span class="sr-only">(current)</span>
                </a>
              </li> 
			  <li class="nav-item">
			  	<a class="nav-link" href="categories.php">Categories</a>
			  </li>
              <li class="nav-item">
			  	<a class="nav-link" href="products.php">Our Products</a>
			  </li>
			 
              <li class="nav-item">
                <a class="nav-link" href="about.html">About Us</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="contact.php">Contact Us</a>
              </li>
			  <li class="nav-item">
			  <?php 
				if (isset($_SESSION['user'])) { ?>

				<img class="my-image img-thumbnail img-circle" src="img.png" alt="" />
				<div class="btn-group my-info">
					<span class="btn btn-default dropdown-toggle" data-toggle="dropdown">
						<?php echo $sessionUser ?>
						<span class="caret"></span>
					</span>
					<ul class="dropdown-menu">
						<li><a href="profile.php">My Profile</a></li>
						<li><a href="newad.php">New Item</a></li>
						<li><a href="profile.php#my-ads">My Items</a></li>
						<li><a href="logout.php">Logout</a></li>
					</ul>
				</div>

				<?php

				} else {
			?>
			<a class="nav-link"  href="login.php">
				Login/Signup
			</a>
			<?php } ?>
			  </li>
            </ul>
          </div>
        </div>
      </nav>
    </header>